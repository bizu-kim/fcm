import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Notification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import util.MSG;

import java.util.HashMap;
import java.util.Map;

public class FirebaseServer {

    private static final Logger logger = LogManager.getLogger(FirebaseServer.class);

    private static String testToken = "ep5lIBGHYRY:APA91bEsbNxp7wR8YGQxyi509ZDtczemkgidH-unZ1skqigJR7muRBC16YkhQc79D_YsY5JQ0IgPnom4IGpcenuh3d4-sqLAI9WPMUUEIHjS3txSjuyFRqLjOTcHvNFDfWOPrnnehVrY";
    private static String testTopic = "testTopic";


    public static void main(String[] args) throws FirebaseMessagingException {
        logger.info("==== Firebase Server Start ====");

        MSG msg = new MSG();

        Map<String, String> testMessage = new HashMap<>();
        testMessage.put("dummy", "value");

        Map<String, String> topic = new HashMap<>();
        topic.put("subscribe", testTopic);

        Map<String, String> unsub = new HashMap<>();
        unsub.put("unsubscribe", testTopic);

        Notification noti = new Notification("test", "body");

        // subscribe and test message
        msg.sendToTokenWithNotification(testToken, noti);
        msg.sendToTopicWithNotification(testTopic, noti, AndroidConfig.builder().putAllData(testMessage).build(), null);

        // unsubscribe and test message
        msg.sendToTopicWithNotification(testTopic, noti, AndroidConfig.builder().putAllData(unsub).build(), null);
        msg.sendToTopicWithNotification(testTopic, noti, AndroidConfig.builder().putAllData(testMessage).build(), null);

        // condition test
        topic.put("subscribe", "industry-tech");
        msg.sendToTokenWithNotification(testToken, noti, AndroidConfig.builder().putAllData(topic).build(), null);
        msg.sendToConditionWithNotification("'industry-tech' in topics && 'bar' in topics", noti);
    }

}

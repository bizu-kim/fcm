package util;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class MSG {

    private static final Logger logger = LogManager.getLogger(MSG.class);

    private static FirebaseApp firebaseApp;
    private static FirebaseMessaging firebaseMessaging;

    public MSG() {
        if (firebaseApp == null) init();
    }

    public static FirebaseMessaging getFirebaseMessaging() {
        if (firebaseMessaging == null) init();
        return firebaseMessaging;
    }

    public static FirebaseApp getFirebaseApp() {
        if (firebaseApp == null) init();
        return firebaseApp;
    }

    private static void init() {
        logger.info("======= Init Firebase =======");
        InputStream serviceAccount = null;
        try {
            serviceAccount = ClassLoader.getSystemResourceAsStream("fcm-test-7c760-firebase-adminsdk-g47bm-6bdf5e39eb.json");

            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .setDatabaseUrl("https://fcm-test-7c760.firebaseio.com/")
                    .build();

            firebaseApp = FirebaseApp.initializeApp(options);
            firebaseMessaging = FirebaseMessaging.getInstance(firebaseApp);

            logger.info("Firebase Initialize Success...");
        } catch (IOException e) {
            logger.error("Firebase Private Key Found Error : {}", e.getMessage());
        }
    }

    /**
     * Send Notification by token
     *
     * @param token        Specific token `ex) ep5lIBGHYRY:APA91bEsbNxp7...hVrY`
     * @param notification Notification `ex) new Notification("title", "body");`
     * @throws FirebaseMessagingException FirebaseMessagingException
     */
    public void sendToTokenWithNotification(String token, Notification notification) throws FirebaseMessagingException {
        send(token, null, null, null, notification, null, null, null);
    }

    /**
     * Send Notification by token
     *
     * @param token         Specific token `ex) ep5lIBGHYRY:APA91bEsbNxp7...hVrY`
     * @param notification  Notification `ex) new Notification("title", "body");`
     * @param androidConfig AndroidConfig
     * @param apnsConfig    ApnsConfig
     * @throws FirebaseMessagingException FirebaseMessagingException
     */
    public void sendToTokenWithNotification(String token, Notification notification, AndroidConfig androidConfig, ApnsConfig apnsConfig) throws FirebaseMessagingException {
        send(token, null, null, null, notification, androidConfig, apnsConfig, null);
    }

    /**
     * Send message by token
     *
     * @param token         Specific token `ex) ep5lIBGHYRY:APA91bEsbNxp7...hVrY`
     * @param data          Data
     * @param notification  Notification `ex) new Notification("title", "body");`
     * @param androidConfig AndroidConfig
     * @param apnsConfig    ApnsConfig
     * @param webpushConfig WebpushConfig
     * @throws FirebaseMessagingException FirebaseMessagingException
     */
    public void sendToToken(String token, Map<String, String> data, Notification notification, AndroidConfig androidConfig,
                            ApnsConfig apnsConfig, WebpushConfig webpushConfig) throws FirebaseMessagingException {
        send(token, null, null, data, notification, androidConfig, apnsConfig, webpushConfig);
    }

    /**
     * Send Notification by topic
     *
     * @param topic        Specific topic name `ex) dm-admin`
     * @param notification Notification `ex) new Notification("title", "body");`
     * @throws FirebaseMessagingException FirebaseMessagingException
     */
    public void sendToTopicWithNotification(String topic, Notification notification) throws FirebaseMessagingException {
        send(null, topic, null, null, notification, null, null, null);
    }

    /**
     * Send message by topic
     *
     * @param topic         Specific topic name `ex) dm-admin`
     * @param notification  Notification `ex) new Notification("title", "body");`
     * @param androidConfig AndroidConfig
     * @param apnsConfig    ApnsConfig
     * @throws FirebaseMessagingException FirebaseMessagingException
     */
    public void sendToTopicWithNotification(String topic, Notification notification, AndroidConfig androidConfig, ApnsConfig apnsConfig) throws FirebaseMessagingException {
        send(null, topic, null, null, notification, androidConfig, apnsConfig, null);
    }

    /**
     * Send message by topic
     *
     * @param topic         Specific topic name `ex) dm-admin`
     * @param data          Data
     * @param notification  Notification `ex) new Notification("title", "body");`
     * @param androidConfig AndroidConfig
     * @param apnsConfig    ApnsConfig
     * @param webpushConfig WebpushConfig
     * @throws FirebaseMessagingException FirebaseMessagingException
     */
    public void sendToTopic(String topic, Map<String, String> data, Notification notification, AndroidConfig androidConfig,
                            ApnsConfig apnsConfig, WebpushConfig webpushConfig) throws FirebaseMessagingException {
        send(null, topic, null, data, notification, androidConfig, apnsConfig, webpushConfig);
    }

    /**
     * Send Notification by condition
     * condition : 'foo' in topics && 'bar' in topics
     *
     * @param condition    Set a condition `ex) foo' in topics && 'bar' in topics`
     * @param notification Notification `ex) new Notification("title", "body");`
     * @throws FirebaseMessagingException FirebaseMessagingException
     */
    public void sendToConditionWithNotification(String condition, Notification notification) throws FirebaseMessagingException {
        send(null, null, condition, null, notification, null, null, null);
    }

    /**
     * Send message by condition
     *
     * @param condition     Set a condition `ex) foo' in topics && 'bar' in topics`
     * @param notification  Notification `ex) new Notification("title", "body");`
     * @param androidConfig AndroidConfig
     * @param apnsConfig    ApnsConfig
     * @throws FirebaseMessagingException FirebaseMessagingException
     */
    public void sendToCondition(String condition, Notification notification, AndroidConfig androidConfig, ApnsConfig apnsConfig) throws FirebaseMessagingException {
        send(null, null, condition, null, notification, androidConfig, apnsConfig, null);
    }

    /**
     * Send message by condition
     *
     * @param condition     Set a condition `ex) 'foo' in topics && 'bar' in topics`
     * @param data          Data
     * @param notification  Notification `ex) new Notification("title", "body");`
     * @param androidConfig AndroidConfig
     * @param apnsConfig    ApnsConfig
     * @param webpushConfig WebpushConfig
     * @throws FirebaseMessagingException FirebaseMessagingException
     */
    public void sendToCondition(String condition, Map<String, String> data, Notification notification, AndroidConfig androidConfig,
                                ApnsConfig apnsConfig, WebpushConfig webpushConfig) throws FirebaseMessagingException {
        send(null, null, condition, data, notification, androidConfig, apnsConfig, webpushConfig);
    }

    /**
     * Send message
     *
     * @param token         Specific token `ex) ep5lIBGHYRY:APA91bEsbNxp7...hVrY`
     * @param topic         Specific topic name `ex) dm-admin`
     * @param condition     Set a condition `ex) foo' in topics && 'bar' in topics`
     * @param data          Data
     * @param notification  Notification `ex) new Notification("title", "body");`
     * @param androidConfig AndroidConfig
     * @param apnsConfig    ApnsConfig
     * @param webpushConfig WebpushConfig
     * @throws FirebaseMessagingException FirebaseMessagingException
     */
    public void send(String token, String topic, String condition, Map<String, String> data, Notification notification,
                     AndroidConfig androidConfig, ApnsConfig apnsConfig, WebpushConfig webpushConfig) throws FirebaseMessagingException {

        Message.Builder message = Message.builder();
        if (token != null) message.setToken(token);
        else if (topic != null) message.setTopic(topic);
        else if (condition != null) message.setCondition(condition);
        else {
            logger.error("Is not set message target.");
            return;
        }
        if (data != null) message.putAllData(data);
        if (notification != null) message.setNotification(notification);
        if (androidConfig != null) message.setAndroidConfig(androidConfig);
        if (apnsConfig != null) message.setApnsConfig(apnsConfig);
        if (webpushConfig != null) message.setWebpushConfig(webpushConfig);

        String response = FirebaseMessaging.getInstance().send(message.build());
        // Response is a message ID string.
        logger.debug("Successfully sent message: {}", response);
    }


////// Below is a list of how to set up the message

    public void sendDryRun() throws FirebaseMessagingException {
        Message message = Message.builder()
                .putData("score", "850")
                .putData("time", "2:45")
                .setToken("token")
                .build();

        // [START send_dry_run]
        // Send a message in the dry run mode.
        boolean dryRun = true;
        String response = FirebaseMessaging.getInstance().send(message, dryRun);
        // Response is a message ID string.
        System.out.println("Dry run successful: " + response);
        // [END send_dry_run]
    }

    public void sendAll() throws FirebaseMessagingException {
        String registrationToken = "YOUR_REGISTRATION_TOKEN";

        // [START send_all]
        // Create a list containing up to 100 messages.
        List<Message> messages = Arrays.asList(
                Message.builder()
                        .setNotification(new Notification("Price drop", "5% off all electronics"))
                        .setToken(registrationToken)
                        .build(),
                // ...
                Message.builder()
                        .setNotification(new Notification("Price drop", "2% off all books"))
                        .setTopic("readers-club")
                        .build()
        );

        BatchResponse response = FirebaseMessaging.getInstance().sendAll(messages);
        // See the BatchResponse reference documentation
        // for the contents of response.
        System.out.println(response.getSuccessCount() + " messages were sent successfully");
        // [END send_all]
    }

    public void sendMulticast() throws FirebaseMessagingException {
        // [START send_multicast]
        // Create a list containing up to 100 registration tokens.
        // These registration tokens come from the client FCM SDKs.
        List<String> registrationTokens = Arrays.asList(
                "YOUR_REGISTRATION_TOKEN_1",
                // ...
                "YOUR_REGISTRATION_TOKEN_n"
        );

        MulticastMessage message = MulticastMessage.builder()
                .putData("score", "850")
                .putData("time", "2:45")
                .addAllTokens(registrationTokens)
                .build();
        BatchResponse response = FirebaseMessaging.getInstance().sendMulticast(message);
        // See the BatchResponse reference documentation
        // for the contents of response.
        System.out.println(response.getSuccessCount() + " messages were sent successfully");
        // [END send_multicast]
    }

    public void sendMulticastAndHandleErrors() throws FirebaseMessagingException {
        // [START send_multicast_error]
        // These registration tokens come from the client FCM SDKs.
        List<String> registrationTokens = Arrays.asList(
                "YOUR_REGISTRATION_TOKEN_1",
                // ...
                "YOUR_REGISTRATION_TOKEN_n"
        );

        MulticastMessage message = MulticastMessage.builder()
                .putData("score", "850")
                .putData("time", "2:45")
                .addAllTokens(registrationTokens)
                .build();
        BatchResponse response = FirebaseMessaging.getInstance().sendMulticast(message);
        if (response.getFailureCount() > 0) {
            List<SendResponse> responses = response.getResponses();
            List<String> failedTokens = new ArrayList<>();
            for (int i = 0; i < responses.size(); i++) {
                if (!responses.get(i).isSuccessful()) {
                    // The order of responses corresponds to the order of the registration tokens.
                    failedTokens.add(registrationTokens.get(i));
                }
            }

            System.out.println("List of tokens that caused failures: " + failedTokens);
        }
        // [END send_multicast_error]
    }

    public Message androidMessage() {
        // [START android_message]
        Message message = Message.builder()
                .setAndroidConfig(AndroidConfig.builder()
                        .setTtl(3600 * 1000) // 1 hour in milliseconds
                        .setPriority(AndroidConfig.Priority.NORMAL)
                        .setNotification(AndroidNotification.builder()
                                .setTitle("$GOOG up 1.43% on the day")
                                .setBody("$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day.")
                                .setIcon("stock_ticker_update")
                                .setColor("#f45342")
                                .build())
                        .build())
                .setTopic("industry-tech")
                .build();
        // [END android_message]
        return message;
    }

    public Message apnsMessage() {
        // [START apns_message]
        Message message = Message.builder()
                .setApnsConfig(ApnsConfig.builder()
                        .putHeader("apns-priority", "10")
                        .setAps(Aps.builder()
                                .setAlert(ApsAlert.builder()
                                        .setTitle("$GOOG up 1.43% on the day")
                                        .setBody("$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day.")
                                        .build())
                                .setBadge(42)
                                .build())
                        .build())
                .setTopic("industry-tech")
                .build();
        // [END apns_message]
        return message;
    }

    public Message webpushMessage() {
        // [START webpush_message]
        Message message = Message.builder()
                .setWebpushConfig(WebpushConfig.builder()
                        .setNotification(new WebpushNotification(
                                "$GOOG up 1.43% on the day",
                                "$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day.",
                                "https://my-server/icon.png"))
                        .build())
                .setTopic("industry-tech")
                .build();
        // [END webpush_message]
        return message;
    }

    public Message allPlatformsMessage() {
        // [START multi_platforms_message]
        Message message = Message.builder()
                .setNotification(new Notification(
                        "$GOOG up 1.43% on the day",
                        "$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day."))
                .setAndroidConfig(AndroidConfig.builder()
                        .setTtl(3600 * 1000)
                        .setNotification(AndroidNotification.builder()
                                .setIcon("stock_ticker_update")
                                .setColor("#f45342")
                                .build())
                        .build())
                .setApnsConfig(ApnsConfig.builder()
                        .setAps(Aps.builder()
                                .setBadge(42)
                                .build())
                        .build())
                .setTopic("industry-tech")
                .build();
        // [END multi_platforms_message]
        return message;
    }

    public void subscribeToTopic() throws FirebaseMessagingException {
        String topic = "highScores";
        // [START subscribe]
        // These registration tokens come from the client FCM SDKs.
        List<String> registrationTokens = Arrays.asList(
                "YOUR_REGISTRATION_TOKEN_1",
                // ...
                "YOUR_REGISTRATION_TOKEN_n"
        );

        // Subscribe the devices corresponding to the registration tokens to the
        // topic.
        TopicManagementResponse response = FirebaseMessaging.getInstance().subscribeToTopic(
                registrationTokens, topic);
        // See the TopicManagementResponse reference documentation
        // for the contents of response.
        System.out.println(response.getSuccessCount() + " tokens were subscribed successfully");
        // [END subscribe]
    }

    public void unsubscribeFromTopic() throws FirebaseMessagingException {
        String topic = "highScores";
        // [START unsubscribe]
        // These registration tokens come from the client FCM SDKs.
        List<String> registrationTokens = Arrays.asList(
                "YOUR_REGISTRATION_TOKEN_1",
                // ...
                "YOUR_REGISTRATION_TOKEN_n"
        );

        // Unsubscribe the devices corresponding to the registration tokens from
        // the topic.
        TopicManagementResponse response = FirebaseMessaging.getInstance().unsubscribeFromTopic(
                registrationTokens, topic);
        // See the TopicManagementResponse reference documentation
        // for the contents of response.
        System.out.println(response.getSuccessCount() + " tokens were unsubscribed successfully");
        // [END unsubscribe]
    }

}
